const isProd = process.env.NODE_ENV === 'production'

export default {}
export const API_URL = !isProd ? 'http://localhost:3010/api/v1/' : 'https://api.magicbook.store/api/v1/'
export const UPLOAD_URL = !isProd ? 'http://127.0.0.1:3003/' : `https://magicbook.store/upload/`

