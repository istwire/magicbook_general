import axios from 'axios'
import JwtService from '@/common/jwt.service'
import { API_URL } from '@/common/config'

axios.defaults.baseURL = API_URL

const ApiService = {
  init () {
    //
  },

  setHeader () {
    axios.defaults.headers.common['Authorization'] = `Token ${JwtService.getToken()}`
  },

  query (resource, params) {
    return axios
      .get(resource, params)
      .catch((error) => {
        throw new Error(`[MB] ApiService ${error}`)
      })
  },

  get (resource, slug = '') {
    return axios
      .get(`${resource}/${slug}`)
      .catch((error) => {
        throw new Error(`[MB] ApiService ${error}`)
      })
  },

  post (resource, params) {
    return axios.post(`${resource}`, params)
  },

  update (resource, slug, params) {
    return axios.put(`${resource}/${slug}`, params)
  },

  put (resource, params) {
    return axios
      .put(`${resource}`, params)
  },

  delete (resource) {
    return axios
      .delete(resource)
      .catch((error) => {
        throw new Error(`[MB] ApiService ${error}`)
      })
  }
}

export default ApiService

export const BookService = {
  create (params) {
    return ApiService.post('media/images', params)
  }
}

export const FieldsService = {
  get (slug) {
    return ApiService.get(`fields/${slug}`)
      .then(({ data }) => data.items)
      .catch((e) => console.log(`[MB] Ошибка при загрузке полей ${slug}. ${e}`))
  }
}

export const OrderService = {
  create (params) {
    return ApiService.post('orders', params)
  }
}

export const CouponsService = {
  check (code) {
    return ApiService.get(`coupons/${code}`)
  }
}
