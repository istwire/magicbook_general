const { resolve } = require('path')

module.exports = {
  /*
  ** Headers of the page
  */
  head: {
    title: 'MagicBook.store',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'MagicBook.store' }
    ],
    link: [
      { rel: 'icon', type: 'image/png', href: '/flower.png' },
      { rel: 'stylesheet', href: 'https://cdn.jsdelivr.net/npm/suggestions-jquery@18.3.3/dist/css/suggestions.min.css' }
    ],
    script: [
      { src: 'https://cdnjs.cloudflare.com/ajax/libs/jquery/1.7/jquery.min.js' },
      { src: 'https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js' },
      { src: 'https://cdn.jsdelivr.net/npm/suggestions-jquery@18.3.3/dist/js/jquery.suggestions.min.js' }
    ]
  },
  plugins: [
    { src: '~/plugins/vue-swal' },
    { src: '~/plugins/turn', ssr: false }
  ],
  css: [
    { src: resolve(__dirname, 'assets/style/common.less'), lang: 'less' },
    { src: './node_modules/swiper/dist/css/swiper.min.css' }
  ],
  /*
  ** Customize the progress bar color
  */
  loading: { color: '#3B8070' },
  /*
  ** Build configuration
  */
  build: {
    vendor: ['vue-swal', 'jquery', 'localforage', '@tweenjs/tween.js'],
    /*
    ** Run ESLint on save
    */
    extend (config, { isDev, isClient }) {
      if (isDev && isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
    }
  }
}
