import {
  ORDER_SAVE,
  ORDER_FETCH,
  ORDER_CLEAR
} from './actions.type'

import {
  ORDER_BILLING,
  ORDER_ADDRESS,
  ORDER_DELIVERY,
  ORDER_PAYMENT,
  ORDER_LOAD,
  ORDER_PURGE
} from './mutations.type'
import localforage from 'localforage'

const state = {
  billing: {
    name: '',
    family: '',
    email: '',
    subscribe: false
  },
  address: {
    postcode: '',
    city: '',
    street: '',
    home: ''
  },
  delivery: {
    _id: '',
    name: '',
    icon: '',
    description: '',
    price: 0
  },
  payment: {
    //
  }
}

const actions = {
  [ORDER_SAVE] ({ state }) {
    return localforage.setItem('order', JSON.stringify(state))
  },
  [ORDER_FETCH] ({ commit }) {
    return localforage.getItem('order')
      .then(order => {
        return commit(ORDER_LOAD, JSON.parse(order))
      })
  },
  [ORDER_CLEAR] ({ commit, dispatch }) {
    localforage.clear()
    commit(ORDER_PURGE)
  }
}

const mutations = {
  [ORDER_BILLING] (state, fields) {
    state.billing = { ...state.billing, ...fields }
  },
  [ORDER_ADDRESS] (state, fields) {
    state.address = { ...state.address, ...fields }
  },
  [ORDER_DELIVERY] (state, fields) {
    state.delivery = { ...state.delivery, ...fields }
  },
  [ORDER_PAYMENT] (state, fields) {
    state.payment = { ...state.payment, ...fields }
  },
  [ORDER_PURGE] (state) {
    const billing = {
      name: '',
      family: '',
      email: '',
      subscribe: false
    }
    state.billing = { ...billing }
    const address = {
      postcode: '',
      city: '',
      street: '',
      home: ''
    }
    state.address = { ...address }
    const delivery = {
      _id: '',
      name: '',
      icon: '',
      description: '',
      price: 0
    }
    state.delivery = { ...delivery }
  },
  [ORDER_LOAD] (state, order) {
    if (!order) return
    state.billing = { ...state.billing, ...order.billing }
    state.address = { ...state.address, ...order.address }
    state.delivery = { ...state.delivery, ...order.delivery }
    state.payment = { ...state.payment, ...order.payment }
  }
}

export default {
  state,
  actions,
  mutations
}
