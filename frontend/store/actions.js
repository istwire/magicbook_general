import { OrderService } from '../common/api.service'
import { ORDER_CREATE } from './actions.type'

export default {
  [ORDER_CREATE]: ({ state, commit, dispatch }) => {
    return OrderService.create({ items: state.cart.items, coupons: state.cart.coupons, ...state.order })
      .catch((e) => {
        console.log(`[MB] Ошибка при создании заказа ${e}`)
      })
  }
}
