import {
  ADD_CART,
  REMOVE_CART,
  CHANGE_QUANTITY,
  UPDATE_ITEM,
  ADD_COUPON,
  UPDATE_SALES,
  CART_LOAD,
  REMOVE_ALL_CART
} from './mutations.type'

import {
  COUPON_CHECK,
  CART_SAVE,
  CART_FETCH,
  CART_CLEAR
} from './actions.type'

import {
  CouponsService
} from '../common/api.service'

import uuid from 'uuid/v4'
import localforage from 'localforage'

const state = {
  items: [],
  coupons: [],
  sales: {
    active: false,
    sale2: 0,
    sale4: 0
  }
}

const getters = {
  sales: state => state.sales
}

const actions = {
  [CART_SAVE] ({ state }) {
    return localforage.setItem('cart', JSON.stringify(state))
  },
  [CART_FETCH] ({ commit }) {
    return localforage.getItem('cart')
      .then(cart => {
        commit(CART_LOAD, JSON.parse(cart))
      })
  },
  [CART_CLEAR] ({ commit, dispatch }) {
    commit(REMOVE_ALL_CART)
    dispatch(CART_SAVE)
  },
  [COUPON_CHECK] ({ commit, state }, code) {
    return new Promise((resolve, reject) => {
      const existsCoupon = state.coupons.find(c => c.code.toLowerCase() === code.toLowerCase())

      if (existsCoupon) {
        reject('Вы уже использовали этот купон')
      }

      CouponsService.check(code)
        .then(({ data }) => {
          if (data.success) {
            commit(ADD_COUPON, data.coupon)
            resolve(data.coupon)
          } else {
            throw new Error('Купон не найден')
          }
        })
        .catch(e => {
          console.log(`[MB] Ошибка при проверке купона ${e}`)
          reject(e)
        })
    })
  }
}

const mutations = {
  [ADD_CART] (state, book) {
    const item = {
      _id: uuid(),
      typo: true,
      ...book
    }

    state.items.push(item)
  },
  [REMOVE_CART] (state, _id) {
    state.items = state.items.filter(i => {
      return i._id !== _id
    })
  },
  [CHANGE_QUANTITY] (state, { _id, quantity }) {
    state.items = state.items.map(i => {
      if (i._id === _id) { i.quantity = quantity }
      return i
    })
  },
  [UPDATE_ITEM] (state, upsetData) {
    state.items = state.items.map(item => {
      if (item._id === upsetData._id) {
        return Object.assign(item, upsetData)
      } else {
        return item
      }
    })
  },
  [ADD_COUPON] (state, coupon) {
    state.coupons = state.coupons.filter(c => c.code !== coupon.code)
    state.coupons.push(coupon)
  },
  [UPDATE_SALES] (state, sales) {
    state.sales = sales
  },
  [CART_LOAD] (state, cart) {
    state.items = cart.items
    state.coupons = cart.coupons
    state.sales = cart.sales
  },
  [REMOVE_ALL_CART] (state) {
    state.items = []
    state.coupons = []
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}
