import {
  FETCH_FORMAT
} from './actions.type'

import {
  FORMAT_UPDATE
} from './mutations.type'

const state = {
  formats: []
}

const getters = {
  formatDefault: state => state.formats[0],
  formats: state => state.formats
}

const actions = {
  [FETCH_FORMAT] () {

  }
}

const mutations = {
  [FORMAT_UPDATE] (state, format) {
    let formats = state.formats.filter(f => f._id !== format._id)
    formats.push(format)
    state.formats = formats
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}
