import Vuex from 'vuex'

import * as getters from './getters'
import actions from './actions'

import book from './book.module'
import modal from './modal.module'
import format from './format.module'
import cart from './cart.module'
import price from './price.module'
import delivery from './delivery.module'
import order from './order.module'

const createStore = () => {
  return new Vuex.Store({
    getters,
    actions,
    modules: {
      book,
      modal,
      format,
      cart,
      price,
      delivery,
      order
    }
  })
}

export default createStore
