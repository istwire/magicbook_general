import {
  MODAL_OPEN,
  MODAL_CLOSE,
  MODAL_TOGGLE,
  MODAL_UPDATE
} from './mutations.type'

import Vue from 'vue'

const state = {
  formats: false,
  name: false,
  message: false,
  cart: false,
  form: false,
  thank: false
}

const getters = {
  //
}

const actions = {
  //
}

const mutations = {
  [MODAL_OPEN] (state, modal) {
    Vue.set(state, modal, true)
  },

  [MODAL_CLOSE] (state, modal) {
    state[modal] = false
  },

  [MODAL_TOGGLE] (state, modal) {
    state[modal] = !state[modal]
  },

  [MODAL_UPDATE] (state, { modal, value }) {
    state[modal] = value
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}

