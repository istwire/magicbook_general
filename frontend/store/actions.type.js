export const BOOK_CREATE = 'bookCreate'

export const FETCH_FORMAT = 'fetchFormat'

export const COUPON_CHECK = 'couponCheck'
export const CART_SAVE = 'cartSave'
export const CART_FETCH = 'cartFetch'
export const CART_CLEAR = 'cartClear'

export const ORDER_CREATE = 'orderCreate'
export const ORDER_SAVE = 'orderSave'
export const ORDER_FETCH = 'orderFetch'
export const ORDER_CLEAR = 'orderClear'
