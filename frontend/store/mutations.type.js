export const BOOK_SAVE = 'bookSave'
export const BOOK_UPDATE = 'bookUpdate'
export const BOOK_RESET = 'bookReset'

export const MODAL_OPEN = 'modalOpen'
export const MODAL_CLOSE = 'modalClose'
export const MODAL_TOGGLE = 'modalToggle'
export const MODAL_UPDATE = 'modalUpdate'

export const FORMAT_UPDATE = 'formatUpdate'

export const ADD_CART = 'cartAdd'
export const REMOVE_CART = 'cartRemove'
export const CHANGE_QUANTITY = 'changeQuantity'
export const UPDATE_ITEM = 'updateItem'
export const ADD_COUPON = 'addCoupon'
export const UPDATE_SALES = 'updateSales'
export const CART_LOAD = 'cartLoad'
export const REMOVE_ALL_CART = 'cartRemoveAll'

export const ORDER_BILLING = 'orderBilling'
export const ORDER_ADDRESS = 'orderAddress'
export const ORDER_DELIVERY = 'orderDelivery'
export const ORDER_PAYMENT = 'orderPayment'
export const ORDER_LOAD = 'orderLoad'
export const ORDER_PURGE = 'orderPurge'

export const DELIVERY_SAVE = 'deliverySave'
