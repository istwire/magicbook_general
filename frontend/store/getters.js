export const book = (state, getters) => {
  if (!state.book.format) {
    return Object.assign(state.book, { format: getters.formatDefault })
  } else {
    return state.book
  }
}

export const cart = state => state.cart.items.map(item => {
  let price = 0
  if (item.typo) {
    price += item.format.price
  }
  if (item.ebook) {
    price += state.price.ebook
  }
  return { ...item, price }
})

export const subtotal = state => state.cart.items.reduce((total, item) => {
  let price = 0
  if (item.typo) {
    price += item.format.price
  }
  if (item.ebook) {
    price += state.price.ebook
  }
  return total + price
}, 0)

export const sale = (state, getters) => {
  let sale = 0
  state.cart.coupons.map(c => {
    if (c.type === 'number') {
      sale -= c.value
    }

    if (c.type === 'percent') {
      sale -= (getters.subtotal + sale) * c.value / 100
    }
  })

  if (state.cart.sales.active) {
    if (state.cart.items.length > 1) {
      sale -= state.cart.sales.sale2
    }
    if (state.cart.items.length > 3) {
      sale -= state.cart.sales.sale4 * state.cart.items.length
    }
  }

  return sale
}

export const total = (state, getters) => {
  const total = getters.subtotal + getters.sale + state.order.delivery.price
  return total > 0 ? total : 0
}
