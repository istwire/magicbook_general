import {
  BOOK_CREATE
} from './actions.type'

import {
  BOOK_RESET,
  BOOK_SAVE,
  BOOK_UPDATE,
  FORMAT_DEFAULT
} from './mutations.type'

import {
  BookService
} from '../common/api.service'

const getters = {
  //
}

const state = {
  letters: '',
  lang: 'ru',
  gender: 'man',
  images: [],
  source: [],
  message: '',
  format: null
}

const actions = {
  [BOOK_CREATE] ({ commit }, { letters = '', lang = 'ru', gender = 'man'}) {
    return BookService.create({ letters, lang, gender  })
      .then(({ data }) => {
        commit(BOOK_UPDATE, { field: 'images', value: data.crop })
        commit(BOOK_UPDATE, { field: 'source', value: data.source })
      })
  }
}

const mutations = {
  [BOOK_SAVE] (state, book) {
    state = Object.assign({}, book)
  },

  [BOOK_UPDATE] (state, { field, value }) {
    if (field === 'letters') {
      if (state.lang === 'ru') {
        value = value.toUpperCase().replace(/[^а-яА-ЯёЁ]/g, '')
      }

      if (state.lang === 'en') {
        value = value.toUpperCase().replace(/[^a-zA-Z]/g, '')
      }

      value = value.substr(0, 12)
    }

    if (field === 'lang' && state.lang !== value) {
      state.letters = ''
    }

    state[field] = value
  },

  [BOOK_RESET] (state) {
    state.letters = ''
    state.images = []
    state.message = ''
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}
