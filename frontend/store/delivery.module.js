import {
  DELIVERY_SAVE
} from './mutations.type'

const state = {
  items: []
}

const getters = {
  deliveryMethods: state => state.items
}

const mutations = {
  [DELIVERY_SAVE] (state, deliveryMethods) {
    state.items = deliveryMethods
  }
}

export default {
  state,
  getters,
  mutations
}
