 module.exports = (mongoose, config) => {
  const database = mongoose.connection
  mongoose.Promise = Promise
  mongoose.connect(config.database, {
    promiseLibrary: global.Promise
  })
  database.on('error', error => console.log(`Connection to MagicBook database failed: ${error}`))
  database.on('connected', () => console.log('Connected to MagicBook database'))
  database.on('disconnected', () => console.log('Disconnected from MagicBook database'))
  process.on('SIGINT', () => {
    database.close(() => {
      console.log('MagicBook terminated, connection closed');
      process.exit(0)
    })
  })
}
