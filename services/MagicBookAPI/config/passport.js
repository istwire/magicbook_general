const PassportJWT = require('passport-jwt')
const ExtractJWT = PassportJWT.ExtractJwt
const Strategy = PassportJWT.Strategy
const config = require('./index.js')
const models = require('@root/app/setup')

module.exports = (passport) => {
  const User = models.User
  const parameters = {
    secretOrKey: config.secret,
    jwtFromRequest: ExtractJWT.fromAuthHeaderAsBearerToken()
  }

  passport.use(new Strategy(parameters, ({ user }, done) => {
    User.findById(user._id)
      .then(user => done(null, user ? user : false))
      .catch(error => {
        done(error, false)
        console.log('error', error)
      })
  }))
}
