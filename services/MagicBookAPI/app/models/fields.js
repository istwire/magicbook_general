const mongoose = require('mongoose')

const Schema = mongoose.Schema({
  slug: {
    type: String,
    required: true
  },
  items: {
    type: Object,
    required: true
  }
})

mongoose.model('Field', Schema)
