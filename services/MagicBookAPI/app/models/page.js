const mongoose = require('mongoose')

const Schema = mongoose.Schema({
  name: {
    type: String,
    default: '',
    required: true
  },
  preview: {
    type: String,
    required: true
  }
})

mongoose.model('Page', Schema)