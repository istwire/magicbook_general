const mongoose = require('mongoose')
const bcrypt = require('bcrypt')

const Schema = mongoose.Schema({
  username: {
    type: String,
    unique: true,
    required: true
  },
  password: {
    type: String,
    required: true
  },
  role: {
    type: String,
    required: true,
    default: 'client'
  }
})

Schema.pre('save', async function (next) {
  const user = this
  if (user.isModified('password') || user.isNew) {
    const salt = await bcrypt.genSalt(10).catch(e => next(e))

    user.password = await bcrypt.hash(user.password, salt).catch(e => next(e))
    next()
  } else {
    return next()
  }
})

Schema.methods.comparePassword = function (password) {
  return bcrypt.compare(password, this.password)
}

mongoose.model('User', Schema)
