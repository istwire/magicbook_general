const mongoose = require('mongoose')

const Schema = mongoose.Schema({
  path: {
    type: String,
    required: true
  },
  lang: {
    type: String,
    enum: ['ru', 'en'],
    default: 'ru'
  },
  age: {
    type: Number,
    enum: [1, 2, 3],
    default: 1
  },
  gender: {
    type: String,
    enum: ['man', 'woman'],
    default: 'man'
  },
  letter: {
    type: String
  },
  pageId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Page'
  }
})

mongoose.model('Media', Schema)
