const mongoose = require('mongoose')

const Schema = mongoose.Schema({
  status: {
    type: String,
    required: true,
    default: 'processing'
  },
  pages: {
    type: Array,
    required: true,
    default: []
  },
  orderId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Order'
  }
})

mongoose.model('Book', Schema)
