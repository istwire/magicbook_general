const mongoose = require('mongoose')
const AutoIncrement = require('mongoose-sequence')(mongoose)

const email = require('./../api/email')

const Schema = mongoose.Schema({
  dateCreated: {
    type: Date,
    default: Date.now,
    required: true
  },
  dateUpdated: {
    type: Date,
    default: Date.now,
    required: true
  },
  items: {
    type: Array,
    default: []
  },
  status: {
    type: String,
    default: ''
  },
  billing: {
    type: Object
  },
  address: {
    type: Object
  },
  delivery: {
    type: Object
  },
  payment: {
    type: Object
  },
  subtotal: {
    type: Number,
    default: 0
  },
  sale: {
    type: Number,
    default: 0
  },
  total: {
    type: Number,
    default: 0
  },
  books: {
    type: Array,
    default: []
  }
})

const handlerUpdateStatus = (order, next) => {
  if (order.isModified('status') && order.status === 'paid') {
    email.newOrderClient(order)
    email.newOrderPrint(order)
  }
  next()
}

Schema.pre('save', async function (next) {
  return handlerUpdateStatus(this, next)
})

Schema.plugin(AutoIncrement, { inc_field: 'orderNumber' })
mongoose.model('Order', Schema)
