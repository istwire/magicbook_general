const basePath = '/api/v1/fields'
const Field = require('@root/app/setup').Field

module.exports = (app) => {
  const api = app.api.field

  app.route(`${basePath}/:slug`)
    .get(api.get(Field))
    .post(api.update(Field))
}
