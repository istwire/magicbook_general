module.exports = (app) => {
  const api = app.api.common

  app.route('/api/v1/build')
    .get(api.build)

  app.route('/api/v1/feedback')
    .post(api.feedback)

  app.route('/api/v1/name')
    .get(api.name)

  app.route('/api/v1/image')
    .get(api.image)
}
