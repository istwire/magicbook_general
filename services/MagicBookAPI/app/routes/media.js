const basePath = '/api/v1/media'
const Media = require('../setup').Media
const Field = require('@root/app/setup').Field
const Page = require('@root/app/setup').Page

module.exports = (app) => {
  const api = app.api.media

  app.route(basePath)
    .get(api.index(Media))
    .post(api.create(Media))

  app.route(`${basePath}/clear`)
    .get(api.clear(Media, Page))

  app.route(`${basePath}/images`)
    .get(api.getImagesByLetters(Media, Field, Page))
    .post(api.getImagesByLetters(Media, Field, Page))

  app.route(`${basePath}/pdf`)
    .get(api.getPdfByLetters(Media, Field, Page))

  app.route(`${basePath}/:id`)
    .get(api.get(Media))
    .post(api.update(Media))
    .delete(api.remove(Media))

  app.route(`${basePath}/batch`)
    .post((req, res) => res.status(501).json({success: true}))
}
