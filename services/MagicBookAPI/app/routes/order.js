const Order = require('@root/app/setup').Order
const Field = require('@root/app/setup').Field

module.exports = (app) => {
  const api = app.api.order

  app.route('/api/v1/orders')
    .get(api.index(Order))
    .post(api.create(Order, Field))

  app.route('/api/v1/orders/count')
    .get(api.count(Order))

  app.route('/api/v1/orders/:id/:book')
    .get(api.book(Order))

  app.route('/api/v1/orders/:id')
    .get(api.get(Order))
    .post(api.update(Order))

  app.route('/api/v1/tinkoff')
    .post(api.tinkoffPayment(Order))

  app.route('/api/v1/subscribers')
      .get(api.subscribers(Order))
}
