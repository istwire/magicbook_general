const basePath = '/api/v1/files'
module.exports = (app) => {
  const api = app.api.file

  app.route(basePath)
    .post(api.upload)
}