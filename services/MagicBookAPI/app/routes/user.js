const passport = require('passport')
const config = require('@config')
const models = require('@root/app/setup')
module.exports = (app) => {
  const api = app.api.user
  app.route('/api/v1/setup')
    .get(api.setup(models.User))

  app.route('/api/v1/users')
    .get(passport.authenticate('jwt', config.session),  api.index(models.User))

  app.route('/api/v1/users/list')
    .get(api.index(models.User))

  app.route('/api/v1/users/:id')
    .get(passport.authenticate('jwt', config.session),  api.get(models.User))
    .post(passport.authenticate('jwt', config.session),  api.update(models.User))
    .delete(passport.authenticate('jwt', config.session),  api.delete(models.User))

  app.route('/api/v1/signup')
    .post(api.signup(models.User))

  app.route('/api/v1/remember')
    .post(api.remember(models.User))
}
