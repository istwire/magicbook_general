const basePath = '/api/v1/pages'
const Page = require('@root/app/setup').Page
const Media = require('@root/app/setup').Media

module.exports = (app) => {
  const api = app.api.page

  app.route(basePath)
    .get(api.index(Page))
    .post(api.create(Page))

  app.route(`${basePath}/name.png`)
    .get(api.name)

  app.route(`${basePath}/:id`)
    .get(api.get(Page, Media))
    .post(api.update(Page))
    .delete(api.remove(Page, Media))

  app.route(`${basePath}/batch`)
    .post((req, res) => {
      res.status(501).json({success: true})
    })
}
