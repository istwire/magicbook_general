module.exports = (app) => {
  const api = app.api.debug

  app.route('/api/v1/debug')
    .get(api.debug)
}
