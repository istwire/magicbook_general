const basePath = '/api/v1/coupons'
const Field = require('@root/app/setup').Field

module.exports = (app) => {
  const api = app.api.coupon

  app.route(`${basePath}/:coupon`)
    .get(api.check(Field))
}
