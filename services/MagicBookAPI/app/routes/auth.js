const passport = require('passport')
const config = require('@config')
const models = require('@root/app/setup')
module.exports = (app) => {
  const api = app.api.auth
  app.route('/')
    .get((req, res) => res.send('MagicAPI API'))

  app.route('/api/v1/auth')
    .get(passport.authenticate('jwt', config.session),  api.check)
    .post(api.login(models.User))
}
