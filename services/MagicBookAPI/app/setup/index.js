const mongoose = require('mongoose')
const UserModel = require('@models/user')
const PageModel = require('@models/page')
const MediaModel = require('@models/media')
const FieldModel = require('@models/fields')
const OrderModel = require('@models/order')
const BookModel = require('@models/book')

const models = {
  User: mongoose.model('User'),
  Page: mongoose.model('Page'),
  Media: mongoose.model('Media'),
  Field: mongoose.model('Field'),
  Order: mongoose.model('Order'),
  Book: mongoose.model('Book'),
}
module.exports = models
