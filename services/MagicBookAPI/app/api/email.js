const api = {}

const emailjs = require('emailjs')
const config = require('@config')

const emailClient = emailjs.server.connect({
  user: config.email.user,
  password: config.email.password,
  host: config.email.host,
  ssl: true,
  timeout: 60000
})

const getTemplate = (name, context) => {
  const Handlebars = require('hbs')
  const fs = require('fs')
  const path = require('path')

  const source = fs.readFileSync(path.join(__dirname, `../email/${name}.hbs`))
  const template = Handlebars.compile(source.toString())

  return template(context)
}

const getItems = (order) => {
  return order.items.map(item => {
    const name = item.letters
    const gender = item.gender === 'man' ? 'мальчик' : 'девочка'
    const lang = item.lang === 'ru' ? 'русский' : 'английский'
    const format = item.format.name
    const link = `${config.apiUrl}/api/v1/orders/${order._id}/${item._id}`
    const typo = item.typo
    const price = item.price

    return {
      name,
      gender,
      lang,
      format,
      pdf: item.ebook,
      price,
      typo,
      link
    }
  })
}

const getAddress = (order) => {
  let address = []
  if (order.address.postcode) {
    address.push({
      label: 'Индекс',
      value: order.address.postcode
    })
  }

  if (order.address.city) {
    address.push({
      label: 'Город',
      value: order.address.city
    })
  }

  if (order.address.street) {
    address.push({
      label: 'Улица',
      value: order.address.street
    })
  }

  if (order.address.home) {
    address.push({
      label: 'Дом',
      value: order.address.home
    })
  }

  address = address.length !== 0 ? address : false

  return address
}

const getCustomer = (order) => {
  return order.billing.name + ' ' + order.billing.family
}

api.newOrderClient = (order) => {
  if (!order.billing.email) { return }

  const email = order.billing.email
  const customer = getCustomer(order)
  const emailData = {
    ...order.toObject(),
    customer,
    items: getItems(order),
    address: getAddress(order)
  }
  const orderHTML = getTemplate( 'newOrderClient', emailData )
  emailClient.send({
    text: '',
    from: `magicbook.store <${config.email.user}>`,
    to: `${customer} <${email}>`,
    bcc: 'alena.vurdova@yandex.ru',
    subject: `Новый заказ ${order.orderNumber}`,
    attachment:
      [
        {data:`<html>${orderHTML}</html>`, alternative:true}
      ]
  }, function(err, msg) { console.log(err); })
}

api.newOrderAdmin = (order) => {
  const emailData = {
    ...order.toObject(),
    customer: getCustomer(order),
    items: getItems(order),
    address: getAddress(order)
  }
  const orderHTML = getTemplate( 'newOrderAdmin', emailData )
  emailClient.send({
    text: '',
    from: `magicbook.store <${config.email.user}>`,
    to: `magicbook.store <${config.email.admin}>`,
    subject: `Новый заказ ${order.orderNumber}`,
    attachment:
      [
        {data:`<html>${orderHTML}</html>`, alternative:true}
      ]
  }, function(err, msg) { console.log(err); })
}

api.newOrderPrint = (order) => {
  const books = getItems(order)
    .filter(book => book.typo)

  if (books.length === 0) {
    return
  }
  const emailData = {
    orderNumber: order.orderNumber,
    books
  }
  const orderHTML = getTemplate( 'newOrderPrint', emailData )
  emailClient.send({
    text: '',
    from: `magicbook.store <${config.email.user}>`,
    to: `magicbook.store <${config.email.admin}>`,
    subject: `Новый заказ на печать книги magicbook.store #${order.orderNumber}`,
    attachment:
      [
        {data:`<html>${orderHTML}</html>`, alternative:true}
      ]
  }, function(err, msg) { console.log(err) })
}

api.feedback = ({ name, tel, comment }) => {
  const emailData = {
    name,
    tel,
    comment
  }
  const orderHTML = getTemplate( 'feedback', emailData )
  return new Promise((resolve, reject) => {
    emailClient.send({
      text: '',
      from: `magicbook.store <${config.email.user}>`,
      to: `magicbook.store <${config.email.admin}>`,
      subject: 'Заявка с сайта',
      attachment:
        [
          { data: `<html>${orderHTML}</html>`, alternative: true }
        ]
      }, (err, msg) => err ? reject(err) : resolve(msg)
    )
  })
}

module.exports = api
