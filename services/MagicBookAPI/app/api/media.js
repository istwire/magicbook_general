const path = require('path')
const flatten = require('lodash.flatten')
const { createBook, getImages  } = require('@root/app/utils/book')
const cropImage = require('@root/app/utils/cropImage')

const api = {}

api.index = (Media) => (req, res) => {
  Media.find({})
    .then(media => res.status(200).json(media))
    .catch(e => res.status(500).json(e))
}

api.create = (Media) => (req, res) => {
  const mediaData = {
    name: req.body.name,
    path: req.body.path,
    letter: req.body.letter,
    lang: req.body.lang,
    age: req.body.age,
    gender: req.body.gender,
    pageId: req.body.pageId
  }
  new Media(mediaData)
    .save()
    .then(media => res.json(media))
    .catch(e => {
      res.status(500).json({ success: false })
      console.log(e)
    })
}

api.get = (Media) => (req, res) => {
  Media.findById(req.params.id)
    .then(media => res.status(200).json(media))
    .catch(e => {
      res.status(500).json({ success: false })
      console.log(e)
    })
}

api.update = (Media) => (req, res) => {
  if (!req.body) return res.status(400).send('Where new data?')

  const mediaData = req.body
  delete mediaData.id
  Media.findByIdAndUpdate(req.params.id, mediaData, { new: true })
    .then(media => {
      if (!media) res.status(400).send('Media not updated')
      res.json({success: true, id: media.id})
    })
    .catch(e => res.status(500).json({text: 'Ошибка при обновлении объекта media', error: e}))
}

api.remove = (Media) => (req, res) => {
  Media.findByIdAndRemove(req.params.id)
    .then(() => res.json({text: `Изображение удалено ${req.params.id}`}))
    .catch(e => res.status(500).json({text: 'Ошибка при удалении объекта media', error: e}))
}


api.getImagesByLetters = (Media, Field, Page) => async (req, res) => {
  const lang = req.query.lang || req.body.lang || 'ru'
  const gender = req.query.gender || req.body.gender || 'man'

  const commonImages = await Field.findOne({ slug: 'commonPage' }).then(page => page.items)

  let before = []
  if (commonImages.firstPage.pdf) {
    before.push([{ path: commonImages.firstPage.pdf, _id: 'before' }])
  }
  let saying = commonImages.before.find(page => page.lang === lang && page.gender === gender)
  before = before.concat(saying.images.map(path => ({ path, _id: 'before' })))

  let after = []
  if (commonImages.lastPage.pdf) {
    after.push([{ path: commonImages.lastPage.pdf, _id: 'after' }])
  }
  let ending = commonImages.after.find(page => page.lang === lang && page.gender === gender)
  after = ending.images.map(path => ({ path, _id: 'after' })).concat(after)

  getImages(Media, Field, Page, req)
    .then(pages => pages.map(p => {
      return p.path.split(';').map(path => {
        return {
          _id: p._id,
          pageId: p.pageId,
          range: p.range,
          path,
          letter: parseInt(p.letter)
        }
      })
    }))
    .then(flatten)
    .then(images => {
      return [...before, ...images, ...after]
    })
    .then(images => {
      const crop = images.map(img => {
        const imgs = cropImage(img.path)
        return [{...img, path: imgs[0]}, {...img, path: imgs[1]}]
      })
      return { crop: flatten(crop), source: images }
    })
    .then(images => res.json(images))
    .catch(e => {
      console.log(e)
      res.status(500).json({success: false, text: 'Что-то пошло не так', error: e})
    })
}

api.getPdfByLetters = (Media, Field, Page) => async (req, res) => {
  const lang = req.query.lang || req.body.lang || 'ru'
  const gender = req.query.gender || req.body.gender || 'man'

  const commonImages = await Field.findOne({ slug: 'commonPage' }).then(page => page.items)
    .catch(e => {
      console.log(e)
      res.status(500).json({ success: false, text: 'Не найдены общие страницы' })
    })

  let before = []
  if (commonImages.firstPage.typo) {
    before.push([{ path: commonImages.firstPage.typo, _id: 'before' }])
  }
  let saying = commonImages.before.find(page => page.lang === lang && page.gender === gender) || []
  before = before.concat(saying.images.map(path => ({ path, _id: 'before' })))

  let after = []
  if (commonImages.lastPage.typo) {
    after.push([{ path: commonImages.lastPage.typo, _id: 'after' }])
  }
  let ending = commonImages.after.find(page => page.lang === lang && page.gender === gender) || []
  after = ending.images.map(path => ({ path, _id: 'after' })).concat(after)

  getImages(Media, Field, Page, req)
    .then(pages => [...before, ...pages, ...after])
    .then(flatten)
    .then(pages => pages.map(p => {
      return p.path.split(';').map(imgPath => {
        // упадёт если по данному пути не будет изображения
        return require.resolve(path.join(__dirname, '../../../../upload/', imgPath))
      })
    }))
    .then(flatten)
    .then(pages => createBook(pages, req.query.letters, req.query.gender))
    .then(stream => {
      res.setHeader('Content-type', 'application/pdf')
      stream.pipe(res)
    })
    .catch(e => {
      console.log(e)
      res.status(500).json({success: false, text: 'Что-то пошло не так', error: e})
    })
}

api.clear = (Media, Page) => (req, res) => {
  Media.find({})
    .then(media => {
      return media.map(async m => {
        const page = await Page.findById(m.pageId)
        if (!page) {
          return await Media.findByIdAndRemove(m._id)
        }
        return []
      })
    })
    .then(() => res.json({ success: true, text: 'Все картинки удалены' }))
}

module.exports = api
