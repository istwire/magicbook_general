const path = require('path')
const { mkdir } = require('shelljs')
const cropImage = require('@root/app/utils/cropImage')
const resize = require('@root/app/utils/resize')

const api = {}

api.upload = (req, res) => {
  if (!req.files) return res.status(400).send('No files were uploaded')

  const file = req.files.file
  const date = new Date()
  const year = date.getFullYear()
  const month = date.getMonth() + 1
  const day = date.getDate()
  const datePath = `${year}/${month}/${day}`
  const uploadPath = path.join(__dirname, '../../../../upload/', datePath)

  // такого пути может не существовать,
  // поэтому его нужно заранее создать
  mkdir('-p', uploadPath)

  file.mv(`${uploadPath}/${file.name}`, e => {
    if (e) return res.status(500).json(e)

    const match = new RegExp(/\.(png|jpg)/g);
    if (match.test(file.name)) {
      cropImage(datePath + '/' + file.name)
      resize(datePath + '/' + file.name, 1000)
    }

    res.json({path: `${datePath}/${file.name}`})
  })
}

module.exports = api
