const api = {}

api.debug = (req, res) => {
  res.json(process.env.NODE_ENV !== 'production')
}


module.exports = api
