const api = {}

api.check = (Field) => (req, res) => {
  Field.findOne({ slug: 'coupons' })
    .then(({ items }) => {
      let coupon = items.find(c => c.code.toLowerCase() === req.params.coupon.toLowerCase())
      if (coupon) {
        res.json({ success: true, coupon })
      } else {
        res.json({ success: false })
      }
    })
    .catch(e => {
      console.log(`Ошибка при загрузку купонов ${e}`)
      res.json({ success: false })
    })
}

module.exports = api
