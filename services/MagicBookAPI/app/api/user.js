const api = {}

api.setup = (User) => (req, res) => {
  User.findOneAndUpdate({ username: 'admin@madgickbook.store' }, { password: 'D8B8JFs3pHem2pA8' })
    .then(u => res.json(u))
    .catch(e => res.json(e))
}

api.index = (User) => async (req, res) => {
  let users = await User.find({}).catch(e => res.status(500).json(e))

  res.json(users)
}

api.signup = (User) => (req, res) => {
  if (!req.body.username || !req.body.password) res.status(401).json({
    success: false,
    message: 'Пожалуйста, введите логин и пароль'
  })
  else {
    const user = new User({
      username: req.body.username,
      password: req.body.password,
      email: req.body.email,
      phone: req.body.phone
    })
    user.save((error) => {
      if (error) return res.status(400).json({ success: false, message: `Пользователь с логином ${user.username} уже существует`, error })
      res.json({ success: true, message: 'Аккаунт успешно создан' })
    })
  }
}

api.get = (User) => (req, res) => {
  User.findById(req.params.id, (error, users) => {
    res.status(200).json(users)
  }).catch(error => {
    res.status(500).json({text: 'Ошибка поиске пользователя', error })
  })
}

api.update = (User) => (req, res) => {
  if (req.params.id !== req.user._id.toString() && req.user.role !== 'admin') {
    return res.status(403).json({ success: false, message: 'Недостаточно прав' })
  }

  const upsertData = req.body
  delete upsertData._id

  User.findOneAndUpdate({ _id: req.params.id }, upsertData, { new: true }, (error, user) => {
    if (error) throw error
    res.status(200).json(user)
  })
}

api.delete = (User) => (req, res) => {
  User.findByIdAndRemove(req.params.id)
    .then(() => res.json({ success: true, message: `Пользователь "${req.params.id}" удалён` }))
    .catch(e => res.json({ success: false, e }))
}

api.remember = (User) => async (req, res) => {
  const user = await User.findOne({ username: req.body.username }).catch(e => res.status(500).json({e}))
  if (!user) return res.status(404).json({ success: false, message: 'User not found'})

  const password = Math.floor(Math.random() * 1000000)

  if (user.email) {
  //  email.remember({ to: user.email, code: password })
  }

  user.password = password
  user.save()

  if (user.email || user.phone) {
    res.json({ success: true, message: 'Новый пароль вам отправлен' })
  } else {
    res.json({ success: true, message: 'В данном профиле не указан почта или телефон' })
  }
}

module.exports = api
