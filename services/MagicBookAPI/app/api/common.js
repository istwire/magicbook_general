const api = {}
const path = require('path')
const email = require('./email')
const { getPageNameTemplate  } = require('@root/app/utils/book')

api.build = (req, res) => {
  const shell = require('shelljs')
  shell.cd(path.join(__dirname, '../../../../frontend'))
  shell.exec('npm run generate')
  shell.exit(1)
  res.json({ success: true })
}

api.feedback = (req, res) => {
  const name = req.body.name
  const tel = req.body.tel
  const comment = req.body.comment

  email.feedback({ name, tel, comment })
    .then(() => {
      res.json({ success: true, message: 'Успешно отправлено' })
    })
    .catch(() => {
      res.json({ success: false, message: 'Не отправлено' })
    })
}

api.name = (req, res) => {
  const { name = '', gender = 'man' } = req.query
  const template = getPageNameTemplate(name, gender)
  res.type('text/html');
  res.status(200);
  res.send(template)
}

api.image = (req, res) => {
  const puppeteer = require('puppeteer')

  (async () => {
    const browser = await puppeteer.launch()
    const page = await browser.newPage()
    const isProd = process.env.NODE_ENV === 'production'
    const apiUrl = isProd ? 'https://api.magicbook.store/api/v1/' : 'http://localhost:3010/api/v1/'
    const { name = '', gender = 'man' } = req.query
    await page.goto(`${apiUrl}name?name=${name}&gender=${gender}`)
    await page.screenshot({ path: 'example.png', fullPage: true })
    await browser.close()
  })()

  res.send({ success: true })
}

module.exports = api
