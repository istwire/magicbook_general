const path = require('path')

const api = {}

api.index = (Field) => (req, res) => {
  Field.find({})
    .then(fields => res.status(200).json(fields))
    .catch(e => res.status(500).json(e))
}

api.create = (Field) => (req, res) => {
  const field = new Field({ slug: req.body.slug, items: req.body.items })
  field.save()
    .then(p => res.json(p))
    .catch(e => res.json(e))
}

api.get = (Field) => (req, res) => {
  Field.findOne({ slug: req.params.slug })
    .then(field => res.status(200).json(field))
    .catch(e => {
      console.log(e)
      res.status(500).json({ success: false })
    })
}

api.update = (Field) => (req, res) => {
  if (!req.body) return res.status(400).send('Where new data?')

  const fieldData = req.body
  delete fieldData._id
  const field = {
    slug: req.params.slug,
    items: fieldData
  }
  Field.findOneAndUpdate({ slug: req.params.slug }, field, { new: true, upsert: true })
    .then(field => res.json(field))
    .catch(e => {
      console.log(e)
      res.status(500).json({text: 'Ошибка при обновлении', error: e})
    })
}

api.remove = (Field) => (req, res) => {
  Field.findByIdAndRemove(req.params.id)
    .then(() => res.json({text: `Страница удалена ${req.params.id}`}))
    .catch(e => res.status(500).json({text: 'Ошибка при удалении', error: e}))
}

module.exports = api
