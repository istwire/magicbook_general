const path = require('path')
const resize = require('@root/app/utils/resize')

const api = {}

api.index = (Page) => (req, res) => {
  Page.find({})
    .then(pages => pages.map(p => Object.assign(p.toObject(), { preview: resize(p.preview, 100) })))
    .then(pages => res.status(200).json(pages))
    .catch(e => res.status(500).json(e))
}

api.create = (Page) => (req, res) => {
  const page = new Page({ name: req.body.name, preview: req.body.preview })
  page.save()
    .then(p => res.json(p))
    .catch(e => res.json(e))
}

api.get = (Page, Media) => (req, res) => {
  Page.findById(req.params.id)
    .then(async page => {
      const images = await Media.find({ pageId: req.params.id })
        .then(media => media.map(m => m.toObject()))
      const p = page.toObject()
      return Object.assign({ images }, p, { preview: resize(p.preview, 1000) })
    })
    .then(page => res.status(200).json(page))
    .catch(e => {
      console.log(e)
      res.status(500).json({ success: false })
    })
}

api.update = (Page) => (req, res) => {
  if (!req.body) return res.status(400).send('Where new data?')

  const pageData = req.body
  delete pageData.id
  Page.findByIdAndUpdate(req.params.id, pageData)
    .then(page => {
      res.json(page)
    })
    .catch(e => {
      res.status(500).json({text: 'Ошибка при обновлении объекта page', error: e})
    })
}

api.remove = (Page, Media) => (req, res) => {
  Page.findByIdAndRemove(req.params.id)
    .then(() => res.json({text: `Страница удалена ${req.params.id}`}))
    .catch(e => res.status(500).json({text: 'Ошибка при удалении объекта page', error: e}))

  Media.findOneAndRemove({ pageId: req.params.id })
}

api.name = (req, res) => {
  const getHalfPageNameTemplate = require('../utils/book').getHalfPageNameTemplate
  const pdf = require('html-pdf')
  const name = req.query.name
  const gender = req.query.gender
  const lastPage = getHalfPageNameTemplate(name, gender)
  const bookUploadDir = path.join(__dirname, '../../../../upload/books/')
  const uploadDir = path.join(__dirname, '../../../../upload/')
  const fileName = `${name}-${gender}`
  const fs = require('fs')

  // если такая страница уже есть, то не генерировать новую
  // if (fs.existsSync(`${uploadDir}crop/right_${name}-${gender}-0.png`)) return res.json({ success: true, status: 'Страница была созданна заранее' })

  const options = {width: '500px', height: '506px', type: 'png'}
/*  const options = {width: '2870px', height: '2906px', type: 'png'}*/
  // const options = { width: '450px', height: '911px', type: 'png', quality: '75' }

  pdf.create(lastPage, options)
    .toFile(`${uploadDir}crop/right_${name}-${gender}-0.png`, (err, response) => {

      if (err) return res.json({ success: false })
      res.json({ success: true, file: response })
    })
/*    .toStream((err, stream) => {
      res.setHeader('Content-type', 'image/png')
      stream.pipe(res)
    })*/
}

module.exports = api
