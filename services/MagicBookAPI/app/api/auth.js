const jwt = require('jsonwebtoken')
const config = require('@config')

const api = {}

api.login = (User) => (req, res) => {
  User.findOne({ username: req.body.username })
    .then((user) => {
      if (!user) return res.status(401).send({ success: false, message: `Автризация провалена. Пользователь '${req.body.username}' не найден` })
      if (user.role === 'banned') { return res.status(401).send({ success: false, message: 'Вы заблокированы' }) }

      user.comparePassword(req.body.password)
        .then((matched) => {
          if (!matched) new Error('Пароль не верный')
          const token = jwt.sign({ user }, config.secret)
          res.json({ success: true, message: 'Token granted', token, user })
        })
        .catch(() => res.status(401).send({ success: false, message: 'Автризация провалена. Пароль не верный.' }))
    })
    .catch(e => {
      console.log(e)
      res.status(500).json(e)
    })
}

api.verify = (headers) => {
  if (headers && headers.authorization) {
    const split = headers.authorization.split(' ')
    if (split.length === 2) return split[1]
    else return null
  } else return null
}

api.check = (req, res) => {
  res.status(200).send({ success: true, message: 'Token valid' })
}

module.exports = api
