const api = {}
const email = require('./email')
const { createBookFile  } = require('@root/app/utils/book')
const path = require('path')
const fs = require('fs')

api.index = (Order) => (req, res) => {
  let orderQuery = {}
  let limit = req.query.limit || 15
  limit = parseInt(limit)
  let skip = 0

  if (req.query.page && limit) {
    skip = req.query.page * limit
  }

  if (req.query.userId) Object.assign(orderQuery, { userId: req.query.userId })
  if (req.query.status) Object.assign(orderQuery, { status: req.query.status })

  Order.find(orderQuery)
    .sort({ dateCreated: 'desc' })
    .skip(skip)
    .limit(limit)
    .then(orders => res.json(orders))
    .catch(e => res.status(403).json({e}))
}

api.count = (Order) => async (req, res) => {
  let orderQuery = {}

  if (req.query.userId) Object.assign(orderQuery, { userId: req.query.userId })
  if (req.query.status) Object.assign(orderQuery, { status: req.query.status })

  res.json(await Order.find(orderQuery).count())
}

api.create = (Order, Field) => async (req, res) => {
  const sales = await Field.findOne({ slug: 'sales' }).then(data => data ? data.items : { active: false }).catch(e => console.log('Ошибка при запросе скидок', e))
  const orderData = req.body
  orderData.status = 'processing'
  const items = orderData.items.map(item => {
    let price = 0
    if (item.typo) { price += item.format.price }
    if (item.ebook) { price += 700 }
    return { ...item, price }
  })
  const subtotal = items.reduce((sum, item) => {
    return sum + item.price
  }, 0)
  let sale = orderData.coupons.reduce((sum, c) => {
    if (c.type === 'number') {
      sum -= c.value
    }
    if (c.type === 'percent') {
      sum -= (subtotal + sum) * c.value / 100
    }
    return sum
  }, 0)

  if (sales.active) {
    if (items.length > 1) {
      sale -= sales.sale2
    }
    if (items.length > 3) {
      sale -= sales.sale4 * items.length
    }
  }

  let total = subtotal + sale + orderData.delivery.price
  total = total > 0 ? total : 0

  let books = items.map(book => {
    return {
      _id: book._id,
      pages: book.source.map(p => path.join(__dirname, '../../../../upload/', p.path)),
      name: book.letters,
      gender: book.gender
    }
  })

  books.map(book => createBookFile(book)
    .then(book => { console.log('Книга создана', book) })
    .catch(e => { console.log('Ошибка при создании книги', e)})
  )

  const order = {
    ...orderData,
    books,
    items,
    subtotal,
    sale,
    total
  }
  Order(order).save()
    .then(order => {
      email.newOrderAdmin(order)

      if (total > 0) {
        return api.tinkoff(order, total)
      } else {
        order.status = 'paid'
        return order.save()
      }
    })
    .then(order => {
      res.json(order)
    })
    .catch(e => {
      console.log('Ошибка при создании заказа', e)
      res.status = 500
      res.json({ status: 'Ошибка при создании заказа', message: e.message })
    })
}

api.get = (Order) => (req, res) => {
  Order.findById(req.params.id)
    .then(order => res.status(200).json(order))
    .catch(e => {
      res.status(500).json({ success: false })
      console.log(e)
    })
}

api.book = (Order) => (req, res) => {
  const bookUploadDir = path.join(__dirname, '../../../../upload/books/')
  Order.findById(req.params.id)
    .then(order => {
      const book = order.books.find(o => o._id === req.params.book)
      if (!book) {
        throw new Error('Такой книги не сущесвует')
      }

      if (fs.existsSync(bookUploadDir + book._id + '.pdf')) {
        const bookPath = bookUploadDir + book._id + '.pdf'
        const file = fs.createReadStream(bookPath);
        const stat = fs.statSync(bookPath);
        res.setHeader('Content-Length', stat.size);
        res.setHeader('Content-Type', 'application/pdf');
        res.setHeader('Content-Disposition', 'attachment; filename=' + book._id + '.pdf');
        file.pipe(res);
      } else {
        createBookFile(book)
        throw new Error('Книга в процессе создания, откройте эту страницу через 5 минут')
      }
    })
    .catch(e => {
      console.log(e)
      res.status(500).send(e.message)
    })
}

api.update = (Order) => (req, res) => {
  Order.findByIdAndUpdate(req.params.id, req.body, { new: true })
    .then(order => { res.json(order) })
    .catch(e => {
      res.status(500).json({ e })
      console.log(e)
    })
}

api.tinkoff = (order, amount) => {
  const config = require('@config')
  const TinkoffMerchantAPI = require('tinkoff-merchant-api')
  const bankApi = new TinkoffMerchantAPI(config.tinkoff.terminalKey, config.tinkoff.secretKey)
  amount = (amount * 100).toString()
  return bankApi.init({
    Amount: amount,
    OrderId: order._id,
    DATA: {
      Email: order.billing.email,
      Phone: order.billing.phone
    },
    Taxation: config.tinkoff.taxation,
    Items: order.items.map(item => {
      let name = 'Книга для ';
      name += item.gender === 'man' ? 'маленького мальчика ' : 'маленькой девочки '
      name += 'по имени ' + item.name
      name += '('
      name += item.typo = 'печатная версия'
      name += item.typo && item.ebook ? ', ' : ''
      name += item.ebook = 'электронная версия'
      name += ')'
      return {
        Name: name,
        Price: item.price,
        Quantity: 1.00,
        Amount: item.price,
        Tax: config.tinkoff.taxation
      }
    })
  })
}

api.tinkoffPayment = (Order) => (req, res) => {
  const orderId = req.body.OrderId
  const statusSource = req.body.Status
  // TODO: добавить проверку что оплаченная сумма = сумме товара в заказе
  const total = req.body.Amount / 100
  let status = 'fail'

  switch (statusSource) {
    case 'CONFIRMED':
      status = 'done'
      break
    case 'AUTHORIZED':
      status = 'processing'
      break
    default:
      status = 'fail'
  }

  Order.findById(orderId)
    .then(order => {
      if (order.status !== 'done') {
        order.status = status
      }
      if (order.status === 'done') {
        email.newOrderClient(order)
        email.newOrderPrint(order)
      }
      return order.save()
    })
    .then(() => res.send('OK'))
    .catch(e => {
      console.log(e)
      res.send('FAIL')
    })
}

api.subscribers = (Order) => async (req, res) => {
  const uniqBy = require('lodash.uniqby')
  const orders = await Order.find()
  const emails = orders
      .filter(order => order.billing.email && order.billing.subscribe)
      .map(order => ({ email: order.billing.email.trim(), dateCreated: order.dateCreated }))

  const cvs = uniqBy(emails, 'email')
      .map(order => `${order.email}, ${order.dateCreated}`)
      .join(`\r\n`)

  res.set('Content-Type', 'text/csv');
  res.set('Content-Disposition', 'attachment; filename=emails.csv');
  res.send(cvs)
}

module.exports = api
