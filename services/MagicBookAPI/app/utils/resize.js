function resize (filePath, width) {
  const path = require('path')
  const fs = require('fs')
  const gm = require('gm')
  const fileAbsPath = path.join(__dirname, '../../../../upload', filePath)
  const fileName = path.basename(filePath)
  const uploadDir = path.join(__dirname, '../../../../upload/crop/')
  const resizeFilePath = uploadDir + width + '_' + fileName

  if (!fs.existsSync(resizeFilePath)) {
    gm(fileAbsPath)
      .resize(width)
      .write(resizeFilePath, (err) => {
        if (err) console.log(`[MB] Ошибка при сохрании изображения ${err}`)
      })
  }

  return `crop/${width}_${fileName}`
}

module.exports = resize
