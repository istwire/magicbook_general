const path = require('path')
const { mkdir } = require('shelljs')
const flatten = require('lodash.flatten')
const bookUploadDir = path.join(__dirname, '../../../../upload/books/')
const assetsDir = path.join(__dirname, '../../assets/')
const fs = require('fs')
mkdir('-p', bookUploadDir)

function getPageNameTemplate (name, gender = 'man', half = false) {
  const css = fs.readFileSync(assetsDir + 'lastPage.css', 'utf8').replace('%assets%', assetsDir)
  const style = `<style>${css}</style>`
  // read binary data
  const bitmap = fs.readFileSync(`${assetsDir}end-${gender}.png`)
  // convert binary data to base64 encoded string
  const base64 = new Buffer(bitmap).toString('base64');
  return `
  ${style}
  <div class="page ${half ? '-half' : ''}">
    <img src="data:image/png;base64,${base64}" alt="">
    <span class="page_name -s${name.length}">${name}</span>
  </div>
  `
}

function getHalfPageNameTemplate (name, gender = 'man', half = false) {
  const css = fs.readFileSync(assetsDir + 'lastPageHalf.css', 'utf8').replace('%assets%', assetsDir)
  const style = `<style>${css}</style>`
  // read binary data
  const bitmap = fs.readFileSync(`${assetsDir}right-end-${gender}.png`)
  // convert binary data to base64 encoded string
  const base64 = new Buffer(bitmap).toString('base64');

  return `
  ${style}
  <div class="page ${half ? '-half' : ''}" style="width: 100%; height: 100%;">
    <img src="data:image/png;base64,${base64}">
    <span class="page_name -s${name.length}">${name}</span>
  </div>
  `
}

async function createBook (pages, name = '', gender = 'man') {
  // на эту магическую цифру нужно умножить, чтобы получить нужные размеры mm -> magic
  // const mmToPdfInch = mm => mm * 2.8421052631578947
  const width = 1379
  const height = 698
  const PDFDocument = require('pdfkit')
  const doc = new PDFDocument({ size: [width, height] })
  for (let i = 0; i < pages.length; i++) {
    doc.image(pages[i], 0, 0, { width, height })
    doc.addPage()
  }

  doc.image(await createNamePageImage(name, gender), 0, 0, { width, height })
  doc.end()

  return doc
}

function createBookFile ({ pages, _id, name, gender }) {
  console.log('fileName', '123')
  const fileName = `${bookUploadDir}${_id}.pdf`
  return createBook(pages, name, gender)
    .then(doc => {
      let writableStream = fs.createWriteStream(fileName)
      doc.pipe(writableStream)
      return fileName
    })
}

function getBookStream (imgsPath, name = '', gender = 'man') {
  return new Promise((resolve, reject) => {
    createBook(imgsPath, name, gender).toStream((err, stream) => {
      if (err) return reject(err)
      resolve(stream)
    })
  })
}

function getImages (Media, Field, Page, req) {
  const uniqBy = require('lodash.uniqby')
  const shuffle = require('lodash.shuffle')
  const alphabet = require('@root/app/utils/alphabet.json')
  const lang = req.query.lang || req.body.lang || 'ru'
  const age = req.query.age || req.body.age || ''
  const gender = req.query.gender || req.body.gender || 'man'
  const letters = req.query.letters || req.body.letters || ''
  // буква -> индекс АБВ -> 123
  const lettersIndex = letters.toUpperCase()
    .split('')
    .map(letter => alphabet[lang].findIndex(l => l === letter))

  let images = []
  for (let i = 0; i < lettersIndex.length; i++) {
    const uuid = require('uuid/v4')()
    image = Media
      .find({ lang, gender, letter: lettersIndex[i] })
      // перемешиваем массив
      .then(shuffle)
      .then(media => {
        return Promise.all(media.map(async m => {
          let path = m.path

          // добавляем подводку
          const before = await Media.findOne({ letter: -1, pageId: m.pageId, gender: m.gender })
          if (before) {
            path = before.path + ';' + path
          }

          return { ...m.toObject(), path, range: uuid }
        }))
      })
    images.push(image)
  }
  return Promise
    .all(images)
    .then(flatten)
    .then(pages => {
      let usedImages = []
      let checkedRange = []
      return pages.filter(page => {
        if (!page.pageId) return false

        if (!usedImages.includes(page.pageId.toString()) && !checkedRange.includes(page.range)) {
          usedImages.push(page.pageId.toString())
          checkedRange.push(page.range)
          return true
        } else {
          return false
        }
      })
    })
    .then(pages => uniqBy(pages, 'range'))
}

const createNamePageImage = async (name, gender) => {
  const puppeteer = require('puppeteer')

  const pathPage = `${bookUploadDir}${name}-${gender}.png`
  const browser = await puppeteer.launch()
  const page = await browser.newPage()
  const isProd = process.env.NODE_ENV === 'production'
  const apiUrl = isProd ? 'https://api.magicbook.store/api/v1/' : 'http://localhost:3010/api/v1/'
  await page.goto(`${apiUrl}name?name=${name}&gender=${gender}`)
  await page.screenshot({ path: pathPage, fullPage: true })
  await browser.close()
  return pathPage
}

module.exports = {
  createBook,
  createBookFile,
  getBookStream,
  getImages,
  getPageNameTemplate,
  getHalfPageNameTemplate,
  createNamePageImage
}
