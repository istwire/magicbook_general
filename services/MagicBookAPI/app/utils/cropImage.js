function cropImage (filePath, abs = false) {
  const path = require('path')
  const fs = require('fs')
  const gm = require('gm')
  const fileAbsPath = !abs ? path.join(__dirname, '../../../../upload', filePath) : filePath
  const fileName = path.basename(filePath)
  const uploadDir = path.join(__dirname, '../../../../upload/crop/')
  const leftFilePath = uploadDir + 'left_' + fileName
  const rightFilePath = uploadDir + 'right_' + fileName

  if (!fs.existsSync(leftFilePath)) {
    gm(fileAbsPath)
      .size(function (err, size) {
        if (err) {
          console.log(`[MB] Ошибка при ресайзе изображения ${err}`)
          return false
        }

        this.crop(size.width / 2, size.height, 0, 0)
          .resize(500)
          .write(leftFilePath, (err) => {
            if (err) console.log(`[MB] Ошибка при сохрании изображения ${err}`)
          })
      })
  }

  if (!fs.existsSync(rightFilePath)) {
    gm(fileAbsPath)
      .size(function (err, size) {
        if (err) {
          console.log(`[MB] Ошибка при ресайзе изображения ${err}`)
          return false
        }

        this.crop(size.width / 2, size.height, size.width / 2, 0)
          .resize(500)
          .write(rightFilePath, (err) => {
            if (err) console.log(`[MB] Ошибка при сохрании изображения ${err}`)
          })
      })
  }

  return ['crop/left_' + fileName, 'crop/right_' + fileName]
}

module.exports = cropImage
