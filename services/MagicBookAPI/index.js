require('module-alias/register')
const http = require('http'),
  MagicBookAPI = require('@MagicBookAPI'),
  MagicBookServer = http.Server(MagicBookAPI),
  PORT = process.env.PORT || 3010,
  HOST = process.env.HOST || '0.0.0.0'

MagicBookServer.listen(PORT, HOST, () => console.log(`MagicBookAPI running on ${HOST}:${PORT}`))