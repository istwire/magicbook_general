const backup = require('mongodb-backup')
const path = require('path')
const dbURI = require('../MagicBookAPI/config/index').database
const mkdir = require('shelljs').mkdir
const root = path.join(__dirname, 'backups', new Date().getTime().toString())

mkdir(root)

backup({
  uri: dbURI,
  root, // write files into this dir
  callback: function(err) {

    if (err) {
      console.error(err)
    } else {
      console.log('finish')
    }
  }
})
