const restore = require('mongodb-restore')
const path = require('path')
const dbURI = require('../MagicBookAPI/config/index').database
const mkdir = require('shelljs').mkdir
const root = path.join(__dirname, 'backups', '1543911677623/admin')

mkdir(root)

restore({
  uri: dbURI,
  root, // write files into this dir
  callback: function(err) {

    if (err) {
      console.error(err)
    } else {
      console.log('finish')
    }
  }
})
