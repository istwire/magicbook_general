const fs = require('fs')
const http = require('http')
const pdf = require('html-pdf')
const tmpl = fs.readFileSync(require.resolve('./index.html'), 'utf8')

const server = http.createServer(function (req, res) {
  if (req.url === '/favicon.ico') return res.end('404')
  const html = tmpl.replace('{{image}}', `file://`)
  pdf.create(html, {width: '56.6cm', height: '28.7cm', border: '3mm'}).toStream((err, stream) => {
    if (err) return res.end(err.stack)
    res.setHeader('Content-type', 'application/pdf')
    stream.pipe(res)
  })

  const alphabet = require('../../MagicBookAPI/app/utils/alphabet')
  for (let i = 0; i < alphabet.ru.length; i++) {
    const html = `
        <html lang="ru">
          <head><meta charset="utf-8"></head>
          <body>
          <section><span>${alphabet.ru[i].toUpperCase()}</span> <span>${alphabet.ru[i].toLowerCase()}</span></section>
          <style>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     
            body { margin: 0; padding: 0; }
            section { position: relative; text-align: center; font-size: 20cm; width: 56cm; height: 28cm; page-break-after: always; justify-content: space-around; display: flex; background-color: lightblue; color: white; }
        
            @media print {
                @page { size: auto; margin: 0; padding: 0; }
                section { position: relative; text-align: center; font-size: 20cm; width: 56cm; height: 28cm; page-break-after: always; }
            }
          </style>
          </body>
        </html>
        `
    const options = {width: '56cm', height: '28cm', type: 'png'}
    pdf.create(html, options).toFile('./letters/'+i+'.png', (err, res) => {
      if (err) return console.log(err);
      console.log(res);
    })
  }
})

server.listen(8090, function (err) {
  if (err) throw err
  console.log('Listening on http://localhost:%s', server.address().port)
})
