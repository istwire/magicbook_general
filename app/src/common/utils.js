import api from '@/api'
import { UPLOAD_URL } from './config'

export function uploadFile (file) {
  if (!file) return

  let formData = new FormData()
  formData.append('file', file)
  const config = {
    headers: { 'content-type': 'multipart/form-data' }
  }
  const url = '/files'
  return api.client.post(url, formData, config)
    .then(({ data }) => data.path)
}

export function getImgSrc (path) {
  return UPLOAD_URL + path
}
