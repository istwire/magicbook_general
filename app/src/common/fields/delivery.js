export default function (order) {
  const data = order.delivery
  let fields = []

  if (!data) {
    return []
  }

  if (data.name) {
    fields.push({
      label: 'Название',
      value: data.name
    })
  }

  if (data.price) {
    fields.push({
      label: 'Цена',
      value: data.price
    })
  }

  return fields
}
