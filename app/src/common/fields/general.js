export default function (order) {
  const data = order.billing
  let fields = []
  if (!data) {
    return []
  }

  if (data.name) {
    fields.push({
      label: 'Имя',
      value: data.name
    })
  }

  if (data.family) {
    fields.push({
      label: 'Фамилия',
      value: data.family
    })
  }

  if (data.email) {
    fields.push({
      label: 'Почта',
      value: data.email
    })
  }

  if (data.phone) {
    fields.push({
      label: 'Телефон',
      value: data.phone
    })
  }

  fields.push({
    label: 'Подписка',
    value: data.subscribe ? 'Да' : 'Нет'
  })

  return fields
}
