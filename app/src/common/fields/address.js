export default function (order) {
  const data = order.address
  let fields = []

  if (!data) {
    return []
  }

  if (data.postcode) {
    fields.push({
      label: 'Индекс',
      value: data.postcode
    })
  }

  if (data.city) {
    fields.push({
      label: 'Город',
      value: data.city
    })
  }

  if (data.street) {
    fields.push({
      label: 'Улица',
      value: data.street
    })
  }

  if (data.home) {
    fields.push({
      label: 'Дом',
      value: data.home
    })
  }

  return fields
}
