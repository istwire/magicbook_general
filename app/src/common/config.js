const isProd = process.env.NODE_ENV === 'production'

export default {}
export const API_URL = isProd ? 'https://api.magicbook.store/api/v1/' : 'http://localhost:3010/api/v1/'
export const UPLOAD_URL = isProd ? 'https://magicbook.store/upload/' : `http://${window.location.hostname}:3003/`
