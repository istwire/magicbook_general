import client from './client'

export default {
  all () {
    return client.get('fields')
      .then(({ data }) => data)
  },

  get (slug) {
    return client.get(`fields/${slug}`)
      .then(({ data }) => {
        return data ? data.items : []
      })
  },

  create (fields) {
    return client.post('fields', fields)
      .then(({ data }) => data)
  },

  update (slug, fields) {
    return client.post(`fields/${slug}`, fields)
      .then(({ data }) => data)
  },

  remove (id) {
    return client.delete(`fields/${id}`)
      .then(({ data }) => data)
  }
}
