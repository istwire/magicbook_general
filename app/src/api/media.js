import client from './client'

export default {
  all () {
    return client.get('media')
      .then(({ data }) => data)
  },

  create (media) {
    return client.post('media', media)
      .then(({ data }) => data)
  },

  get (id) {
    return client.get(`/media/${id}`)
      .then(({ data }) => data)
  },

  update (media) {
    if (media) {
      return client.post(`media/${media._id}`, media)
        .then(({ data }) => data)
    } else {
      return client.delete(`media/${media._id}`)
        .then(({ data }) => data)
    }
  },

  remove (media) {
    return client.delete(`media/${media._id}`)
      .then(({ data }) => data)
  }
}
