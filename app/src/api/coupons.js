import client from './client'

export default {
  all () {
    return client.get('coupons')
      .then(({ data }) => data)
  },

  get (slug) {
    return client.get(`coupons/${slug}`)
      .then(({ data }) => {
        return data ? data.items : []
      })
  },

  create (coupons) {
    return client.post('coupons', coupons)
      .then(({ data }) => data)
  },

  update (slug, coupons) {
    return client.post(`coupons/${slug}`, coupons)
      .then(({ data }) => data)
  },

  remove (id) {
    return client.delete(`coupons/${id}`)
      .then(({ data }) => data)
  }
}
