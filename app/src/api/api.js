import client from './client'
import media from './media'
import page from './page'

export default {
  client,
  media,
  page
}
