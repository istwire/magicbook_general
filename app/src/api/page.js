import client from './client'

export default {
  all () {
    return client.get('pages')
      .then(({ data }) => data)
  },

  get (id) {
    return client.get(`pages/${id}`)
      .then(({ data }) => data)
  },

  create (page) {
    return client.post('pages', page)
      .then(({ data }) => data)
  },

  update (page) {
    return client.post(`pages/${page._id}`, page)
      .then(({ data }) => data)
  },

  remove (id) {
    return client.delete(`pages/${id}`)
      .then(({ data }) => data)
  }
}
