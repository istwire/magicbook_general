import Vue from 'vue'
import axios from 'axios'
import VueAxios from 'vue-axios'
import JwtService from '@/common/jwt.service'
import { API_URL } from '@/common/config'
import VueCookie from 'vue-cookie'

const ApiService = {
  init () {
    Vue.use(VueAxios, axios)
    Vue.axios.defaults.baseURL = API_URL
  },

  setHeader () {
    Vue.axios.defaults.headers.common['Authorization'] = `Bearer ${JwtService.getToken()}`
  },

  query (resource, params) {
    return Vue.axios
      .get(resource, params)
      .catch((error) => {
        throw new Error(`[RWV] ApiService ${error}`)
      })
  },

  get (resource, slug = '') {
    return Vue.axios
      .get(`${resource}/${slug}`)
      .catch((error) => {
        throw new Error(`[RWV] ApiService ${error}`)
      })
  },

  post (resource, params) {
    return Vue.axios.post(`${resource}`, params)
  },

  update (resource, slug, params) {
    return Vue.axios.put(`${resource}/${slug}`, params)
  },

  put (resource, params) {
    return Vue.axios
      .put(`${resource}`, params)
  },

  delete (resource) {
    return Vue.axios
      .delete(resource)
      .catch((error) => {
        throw new Error(`[RWV] ApiService ${error}`)
      })
  }
}

ApiService.init()
ApiService.setHeader()

export default ApiService

export const TagsService = {
  get () {
    return ApiService.get('tags')
  }
}

export const ArticlesService = {
  query (type, params) {
    return ApiService
      .query(
        'articles' + (type === 'feed' ? '/feed' : ''),
        { params: params }
      )
  },
  get (slug) {
    return ApiService.get('articles', slug)
  },
  create (params) {
    return ApiService.post('articles', {article: params})
  },
  update (slug, params) {
    return ApiService.update('articles', slug, {article: params})
  },
  destroy (slug) {
    return ApiService.delete(`articles/${slug}`)
  }
}

export const CommentsService = {
  get (slug) {
    if (typeof slug !== 'string') {
      throw new Error('[RWV] CommentsService.get() article slug required to fetch comments')
    }
    return ApiService.get('articles', `${slug}/comments`)
  },

  post (slug, payload) {
    return ApiService.post(
      `articles/${slug}/comments`, { comment: { body: payload } })
  },

  destroy (slug, commentId) {
    return ApiService
      .delete(`articles/${slug}/comments/${commentId}`)
  }
}

export const FavoriteService = {
  add (slug) {
    return ApiService.post(`articles/${slug}/favorite`)
  },
  remove (slug) {
    return ApiService.delete(`articles/${slug}/favorite`)
  }
}

export const UserService = {
  reg (user) {
    return ApiService.post('signup', user)
  },

  auth (user) {
    return ApiService.post('auth', user)
      .then(({ data }) => {
        if (!data.token) return new Error('Сервер не вернул токен')
        JwtService.saveToken(data.token)
        VueCookie.set('madgick_user_id', data.user._id, '1D')
        VueCookie.set('madgick_user_role', data.user.role, '1D')
        return data
      })
  },

  checkAuth () {
    // return ApiService.get('auth')
    return new Promise((resolve, reject) => {
      if (JwtService.getToken()) {
        resolve()
      } else {
        reject(new Error('Токен не найден'))
      }
    })
  }
}
