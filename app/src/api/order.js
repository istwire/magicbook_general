import client from './client'

export default {
  all () {
    return client.get('orders')
      .then(({ data }) => data)
  },

  create (media) {
    return client.post('orders', media)
      .then(({ data }) => data)
  },

  get (id) {
    return client.get(`/orders/${id}`)
      .then(({ data }) => data)
  },

  update (order) {
    return client.post(`orders/${order._id}`, order)
      .then(({ data }) => data)
  },

  remove (order) {
    return client.delete(`orders/${order._id}`)
      .then(({ data }) => data)
  }
}
