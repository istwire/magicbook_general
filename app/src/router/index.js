import Vue from 'vue'
import Router from 'vue-router'
import { UserService } from '@/api/api.service'

import Home from '@/pages/Home'
import Media from '@/pages/Media'
import MediaNew from '@/pages/MediaNew'
import MediaEdit from '@/pages/MediaEdit'
import Page from '@/pages/Page'
import PageEdit from '@/pages/PageEdit'
import PageNew from '@/pages/PageNew'
import Authentication from '@/pages/Authentication/Authentication'
import Fields from '@/pages/Fields'
import Orders from '@/pages/Orders'
import Order from '@/pages/Order'
import Coupons from '@/pages/Coupons'

Vue.use(Router)

const router = new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home,
      meta: {
        requiredAuth: true
      }
    },
    {
      path: '/media/new',
      name: 'MediaNew',
      component: MediaNew,
      meta: {
        requiredAuth: true
      }
    },
    {
      path: '/media',
      name: 'Media',
      component: Media,
      meta: {
        requiredAuth: true
      }
    },
    {
      path: '/media/:id',
      name: 'MediaEdit',
      component: MediaEdit,
      meta: {
        requiredAuth: true
      }
    },
    {
      path: '/page',
      name: 'Page',
      component: Page,
      meta: {
        requiredAuth: true
      }
    },
    {
      path: '/page/new',
      name: 'PageNew',
      component: PageNew,
      meta: {
        requiredAuth: true
      }
    },
    {
      path: '/page/:id',
      name: 'PageEdit',
      component: PageEdit,
      meta: {
        requiredAuth: true
      }
    },
    {
      path: '/login',
      name: 'Authentication',
      component: Authentication
    },
    {
      path: '/setting/:slug',
      name: 'Fields',
      component: Fields,
      meta: {
        requiredAuth: true
      }
    },
    {
      path: '/coupons',
      name: 'Coupons',
      component: Coupons,
      meta: {
        requiredAuth: true
      }
    },
    {
      path: '/orders',
      name: 'Orders',
      component: Orders,
      meta: {
        requiredAuth: true
      }
    },
    {
      path: '/orders/:id',
      name: 'Order',
      component: Order,
      meta: {
        requiredAuth: true
      }
    }
  ]
})

router.beforeEach((to, from, next) => {
  if (to.meta && to.meta.requiredAuth) {
    UserService
      .checkAuth()
      .then(() => next())
      .catch((e) => {
        console.log(e)
        next('/login')
      })
  }
  next()
})

export default router
