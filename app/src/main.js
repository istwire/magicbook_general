// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import Vuetify from 'vuetify'
import LayoutGeneral from '@/layouts/General'
import wysiwyg from 'vue-wysiwyg'
import VueSwal from 'vue-swal'
import('vuetify/dist/vuetify.min.css')
import('vue-wysiwyg/dist/vueWysiwyg.css')
Vue.config.productionTip = false

Vue.use(Vuetify)
Vue.use(VueSwal)

Vue.use(wysiwyg, {})

Vue.component('l-general', LayoutGeneral)

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  template: '<App/>',
  components: { App }
})
